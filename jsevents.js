window.onload = () =>{

    // 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el
    // evento click que ejecute un console log con la información del evento del click

    // const btnToClick = document.getElementById('btnToClick');
    // btnToClick.addEventListener('click', consoleLog);

    // function consoleLog () {
    //     console.log('has hecho click')
    // }

    // 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.

    // const inputFocus = document.querySelector('.focus');

    // inputFocus.addEventListener('focus', muestraElInput);

    // function muestraElInput (){
    //     let inputValue = inputFocus.value;
    //     console.log(inputValue)
    // }

    // 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.

    const inputFocus = document.querySelector('.focus');

    inputFocus.addEventListener('input', muestraElInput);

    function muestraElInput (){
        let inputValue = inputFocus.value;
        console.log(inputValue)
    }


}